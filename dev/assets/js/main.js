const breed = document.querySelector('.section-breed');
const name = document.querySelector('.section-name');
const font = document.querySelector('.section-font');
const colorFont = document.querySelector('.section-color-font');
const finalPage = document.querySelector('.final-page');
const clearLocalStorage = document.querySelector('.clearLocalStorage');

if (clearLocalStorage) {
    clearLocalStorage.addEventListener('click', (e) => {
        e.preventDefault();
        localStorage.clear();
        window.location.href = 'index.html';
    })
}

if (breed) {
    const selectBreed = document.querySelector('.dog__breed');
    const buttonBreed = document.querySelector('.buttons--breed');
    const apiUrl = "https://dog.ceo/api/breeds/list/all";

    fetch(apiUrl)
        .then(
            response => {
                if (response.status !== 200) {
                    console.error(`Erro: ${response.status}`);
                    return;
                }

                response.json().then(
                    function (data) {
                        const cleanResult = /\[\"|"|]/g;
                        let breedType = [];
                        let breedTypeJson = 0;
                        let optionBreed;
                        let optionBreedType;

                        for (let i = 0; i < Object.keys(data.message).length; i++) {

                            optionBreed = document.createElement('option');
                            optionBreed.text = Object.keys(data.message)[i];
                            optionBreed.value = optionBreed.text;

                            if (Object.values(data.message)[i].length > 0) {
                                breedTypeJson = JSON.stringify(Object.values(data.message)[i]);
                                breedType = breedTypeJson.split(",");
                                for (let j = 0; j < breedType.length; j++) {
                                    optionBreedType = document.createElement('option');
                                    optionBreedType.text = `${Object.keys(data.message)[i]} - ${breedType[j].toString().replace(cleanResult, "")}`;
                                    optionBreedType.value = optionBreedType.text;
                                    selectBreed.add(optionBreedType);
                                }

                            }
                            else {
                                selectBreed.add(optionBreed);
                            }
                        }
                    }
                );

            }
        )
        .catch((errorEvent) => console.error(`Erro no FetchAPI = ${errorEvent}`));

    buttonBreed.addEventListener('click', (e) => {
        if (selectBreed.selectedIndex !== 0) {
            e.preventDefault();
            let dogBreed = selectBreed.value.trim();
            dogBreed = dogBreed.toLocaleLowerCase();
            dogBreed = dogBreed.replace(' - ', "/");
            const urlImg = `https://dog.ceo/api/breed/${dogBreed}/images/random`;

            localStorage.setItem("urlRandomPhoto", urlImg);
            localStorage.setItem("dogBreed", dogBreed);

            window.location.href = 'passo-2.html';
        } else {
            alert('Favor escolher um raça.')
        }
    })
}

if (name) {
    const buttonName = (document.querySelector('.buttons--name')) ? document.querySelector('.buttons--name') : document.createElement('button');
    buttonName.addEventListener('click', (e) => {
        e.preventDefault();
        const dogName = document.querySelector('.dog__name').value;
        if (dogName) {
            localStorage.setItem("dogName", dogName);
            window.location.href = 'passo-3.html';
        } else {
            alert('Favor escolher um nome.')
        }
    })
}

if (font) {
    const buttonFont = document.querySelector('.buttons--font');
    buttonFont.addEventListener('click', (e) => {
        e.preventDefault();
        const selectFont = document.querySelector('.dog__font');
        if (selectFont.selectedIndex !== 0) {
            localStorage.setItem("dogFont", selectFont.value);
            window.location.href = 'passo-4.html';
        } else {
            alert('Favor escolher uma fonte.')
        }
    })
}

if (colorFont) {
    const buttonColorFont = document.querySelector('.buttons--color');
    let dogPhoto = localStorage.getItem("urlRandomPhoto");
    fetch(dogPhoto)
        .then(
            dogPhotoResponse => {
                if (dogPhotoResponse.status !== 200) {
                    console.error(`Erro na resposta da API: ${dogPhotoResponse.status}`);
                    return;
                }
                dogPhotoResponse.json().then(
                    function (dataImg) {
                        localStorage.setItem("dogPhoto", dataImg.message);
                    }

                )
            }
        )
        .catch(function (errorEvent) {
            console.error(`Erro no FetchAPI = ${errorEvent}`);
        });

    buttonColorFont.addEventListener('click', (e) => {
        e.preventDefault();
        const selectFontColor = document.querySelector('.dog__color-font');
        if (selectFontColor.selectedIndex !== 0) {
            localStorage.setItem("fontColor", selectFontColor.value);
            moment.locale('pt-br');
            const agora = moment().format('MMMM D YYYY, hh:mm:ss');
            localStorage.setItem("dateTime", agora);
            window.location.href = 'final.html';
        } else {
            alert('Favor escolher uma cor para a fonte.')
        }
    })
}

if (finalPage) {
    const dogBreed = localStorage.getItem("dogBreed");
    const dogName = localStorage.getItem("dogName");
    const fontColor = localStorage.getItem("fontColor");
    const dogFont = localStorage.getItem("dogFont");
    const dateTime = localStorage.getItem("dateTime");
    const dogPhoto = localStorage.getItem("dogPhoto");
    const dogData = document.querySelector('.item__caption');
    const dateTimeWrapper = document.querySelector('.sucess-message__datetime');

    const img = document.createElement('img');
    img.src = dogPhoto;
    img.alt = dogName
    document.querySelector('.item__polaroid').appendChild(img);

    dogData.style.fontFamily = dogFont;
    dogData.style.color = fontColor;
    dogData.innerHTML = (
        `Nome: ${dogName} <br/>Raça: ${dogBreed}`
    );

    dateTimeWrapper.innerHTML = dateTime;
}
