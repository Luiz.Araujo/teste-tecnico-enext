# Challenge Enext • Front-End

## Objetivo
Buscar em uma API pública todas as raças de cachorros.

O usuário poderá digitar o nome do cachorro, estilizar a fonte e a cor e essa informação deverá ser armazenada no local storage.

* Documentação Dog API: [Dog API](https://dog.ceo/dog-api/).

* [Demais especificações do projeto](https://github.com/enext-wpp/challenges/tree/master/challenge-two).


### Configuração do projeto

#### 1 - Node e NPM

Para o funcionamento do projeto é necessário ter o Node e o NPM instalados na máquina, use o comando abaixo para verificar se já estão instalados.

```sh
node --version
npm --version
```
Se necessário [clique aqui](https://nodejs.org/en/) e realize a instalação da versão LTS seguindo os procedimentos para seu sistema operacional.

#### 2 - Gulp
O projeto utiliza o Gulp como seu task runner padrão, para isso verifique se ele já está instalado em sua máquina.

```sh
gulp --version
```
Se for preciso instalar siga o comando abaixo:

```sh
npm install --global gulp-cli
```

#### 3 - Download do projeto
Agora é a hora de clonar o projeto ou efetuar o download, para isso [clique aqui](https://gitlab.com/Luiz.Araujo/teste-tecnico-enext).


#### 4 - Dependências do projeto

Após isso basta acessar a pasta do projeto e executar o comando abaixo, que as dependências serão instaladas.

```sh
npm i
```

## Scripts automatizados

### Build
Realiza as tarefas necessárias para preparar os arquivos de produção, este arquivos serão criados na pasta **/prod** na raiz do projeto.

```sh
gulp
```

### Desenvolvimento
Cria o ambiente de desenvolvimento, criando um servidor local.
```sh
gulp serve
```