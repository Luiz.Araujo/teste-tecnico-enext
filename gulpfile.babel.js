// Include Gulp & Tools We'll Use
import gulp from 'gulp';
import gutil from 'gulp-util';
import gulpLoadPlugins from 'gulp-load-plugins';
import del from 'del';
import runSequence from 'run-sequence';
import browserSync from 'browser-sync';
import beep from 'beepbeep';
import cleanCSS from 'gulp-clean-css';
import log from 'fancy-log';
import sass from 'gulp-sass';

sass.compiler = require('node-sass');

const $ = gulpLoadPlugins({
  pattern: ['gulp-*', 'gulp.*'],
  replaceString: /^gulp(-|\.)/,
});

const basePaths = {
  rootDev: 'dev/',
  rootProd: 'prod/',
  src: 'dev/assets/',
  dest: 'prod/assets/',
  tmp: '.tmp/',
};

const paths = {
  images: {
    src: `${basePaths.src}img/`,
    dest: `${basePaths.dest}img/`,
  },
  scripts: {
    src: `${basePaths.src}js/`,
    dest: `${basePaths.dest}js/`,
  },
  styles: {
    src: `${basePaths.src}sass/`,
    srcCss: `${basePaths.src}css/`,
    dest: `${basePaths.dest}css/`,
  },
};

const appFiles = {
  styles: `${paths.styles.src}**/*.scss`,
};

// Allows gulp --dev to be run for a more verbose output
let isProduction = true;
let sassStyle = 'compressed';
let sourceMap = false;

if (gutil.env.dev === true) {
  sassStyle = 'expanded';
  sourceMap = true;
  isProduction = false;
}

const changeEvent = function (evt) {
  gutil.log(
    'File: ',
    gutil.colors.cyan(evt.path.replace(new RegExp(`/.*(?=/ + ${basePaths.src} + )/`), '')),
    'was',
    gutil.colors.magenta(evt.type),
  );
};

// function with log error
const onError = function (err) {
  beep([0, 0, 0]);
  gutil.log(gutil.colors.green(err));
  this.emit('end');
};

const AUTOPREFIXER_BROWSERS = ['last 2 version'];

// Copy All Files At The Root Level (assets)
gulp.task('copy', (cb) => {
  gulp
    .src([
      `${basePaths.rootDev}**/img/**`
    ])
    .pipe(gulp.dest(basePaths.rootProd))
    .on('end', cb)
    .on('error', cb)
    .pipe($.size({ title: 'copy' }));
});

// Automatically Prefix CSS
gulp.task('styles:css', (cb) => {
  gulp
    .src(`${paths.styles.srcCss}**/*.css`)
    .pipe($.plumber({
      errorHandler: onError,
    }))
    .pipe($.newer(`${paths.styles.srcCss}**/*.css`))
    .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
    .pipe($.csscomb())
    .pipe(gulp.dest(paths.styles.srcCss))
    .on('end', cb)
    .on('error', cb)
    .pipe($.size({ title: 'estilos:css' }));
});

// Compile Any Other Sass Files You Added (app/styles)
gulp.task('styles:scss', (cb) => {
  gulp
    .src(appFiles.styles)
    .pipe($.plumber({
      errorHandler: onError,
    }))
    .pipe($.sourcemaps.init())
    .pipe($.wait(500))
    .pipe(sass().on('error', sass.logError))
    .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
    .pipe(isProduction
      ? cleanCSS({ rebase: false }, (details) => {
        log(`Detalhes arquivo original   -->> ${details.name} : ${details.stats.originalSize}`);
        log(`Detalhes arquivo minificado -->> ${details.name} : ${details.stats.minifiedSize}`);
      })
      : gutil.noop())
    .pipe($.sourcemaps.write('./maps'))
    .pipe(gulp.dest(`${basePaths.tmp}css`))
    .pipe(gulp.dest(`${paths.styles.srcCss}`))
    .on('end', cb)
    .on('error', cb)
    .on('error', (err) => {
      gutil.PluginError('CSS', err, {
        showStack: true,
      });
    })
    .pipe($.size({ title: 'styles:scss' }));
});

// Output Final CSS Styles
gulp.task('styles', ['styles:scss', 'styles:css']);

// Build aplication Scan Your HTML For Assets & Optimize Them
gulp.task('build', ['styles:scss'], (cb) => {
  gulp
    .src([
      `${basePaths.rootDev}**/*.html`,
    ])
    .pipe($.plumber({
      errorHandler: onError,
    }))
    // Concatenate And Minify JavaScript
    .pipe(gutil.env.dev === 'true' ? $.sourcemaps.init() : gutil.noop()) // Gera o sourcemap apenas em desenvolvimento (Ex: gulp --dev true build)
    .pipe($.useref({ searchPath: `{${basePaths.tmp},${basePaths.rootDev}}` }))
    .pipe($.if('*.js', $.babel({ presets: ['@babel/preset-env'] })))
    .pipe($.if('*.js', $.stripDebug()))
    .pipe($.if('*.js', $.uglify()))
    .pipe(gutil.env.dev === 'true' ? $.sourcemaps.write() : gutil.noop()) // Gera o sourcemap apenas em desenvolvimento (Ex: gulp --dev true build)
    // Concatenate And Minify Styles
    .pipe(gutil.env.dev === 'true' ? $.sourcemaps.init() : gutil.noop()) // Gera o sourcemap apenas em desenvolvimento (Ex: gulp --dev true build)
    .pipe($.if(
      '*.css',
      cleanCSS({ rebase: false }, (details) => {
        log(`Detalhes arquivo original   -->> ${details.name} : ${details.stats.originalSize}`);
        log(`Detalhes arquivo minificado -->> ${details.name} : ${details.stats.minifiedSize}`);
      }),
    ))
    .pipe($.useref({ searchPath: `{${basePaths.tmp},${basePaths.rootDev}}` }))
    .pipe(gutil.env.dev === 'true' ? $.sourcemaps.write() : gutil.noop()) // Gera o sourcemap apenas em desenvolvimento (Ex: gulp --dev true build)
    .pipe(gulp.dest(basePaths.rootProd))
    .on('end', cb)
    .on('error', cb)
    .pipe($.size({
      title: 'build',
      showFiles: true,
      showTotal: true,
      pretty: true,
    }));
});

// Clean Output Directory
gulp.task('clean', del.bind(null, ['.tmp', 'prod']));

// Watch Files For Changes & Reload
gulp.task('serve', () => {
  browserSync({
    notify: false,
    server: {
      baseDir: [basePaths.tmp, basePaths.rootDev],
      index: 'index.html',
    },
  });

  gulp.watch([`${basePaths.rootDev}**/*.html`], browserSync.reload).on('change', (evt) => {
    changeEvent(evt);
  });
  gulp
    .watch([`${paths.styles.src}**/*.scss`], ['styles:scss', browserSync.reload])
    .on('change', (evt) => {
      changeEvent(evt);
    });
  gulp
    .watch([`{${basePaths.tmp},${basePaths.src}}/css/*.css`], ['styles:css', browserSync.reload])
    .on('change', (evt) => {
      changeEvent(evt);
    });
  gulp.watch([`${paths.images.src}**/*`], browserSync.reload).on('change', (evt) => {
    changeEvent(evt);
  });
});

// Build Production Files, the Default Task
gulp.task('default', ['clean'], (cb) => {
  runSequence('styles', ['build', 'copy'], cb);
});
